//
//  ViewController.m
//  Learning0514
//
//  Created by  apple on 13-5-14.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize nameField;
@synthesize numberField;
@synthesize sliderLabel;
@synthesize leftSwitch;
@synthesize rightSwitch;
@synthesize doSomethingButton;


//文本框输入完成时， 关闭键盘
-(IBAction)textFieldDoneEditing:(id)sender{
    //取消第一响应者状态（第一响应者： 用户当前与之交互的控件）
    [sender resignFirstResponder];
}

//点击背景时，关闭键盘
-(IBAction)backgroundTap:(id)sender{
    [nameField resignFirstResponder];
    [numberField resignFirstResponder];
}

//滑块滑动时，显示当前的值
-(IBAction)sliderChanged:(id)sender{
    UISlider *slider = (UISlider *)sender;
    int progressAsInt = (int)(slider.value + 0.5f); //四舍五入
    NSString *newText = [[NSString alloc] initWithFormat:@"%d", progressAsInt];
    sliderLabel.text = newText;
    [newText release];
}

//分段控件，切换显示不同的控件
-(IBAction)toggleControls:(id)sender{
    //index为0时，显示开关，否则，显示按钮
    if([sender selectedSegmentIndex] == kSwitchesSegmentIndex){
        leftSwitch.hidden = NO;
        rightSwitch.hidden = NO;
        doSomethingButton.hidden = YES;
    }else{
        leftSwitch.hidden = YES;
        rightSwitch.hidden = YES;
        doSomethingButton.hidden = NO;
    }
}

//开关切换on\off
-(IBAction)switchChanged:(id)sender{
    UISwitch *whichSwitch = (UISwitch *)sender;
    BOOL setting = whichSwitch.isOn;
    //两个开关一起动，仅做测试
    [leftSwitch setOn:setting animated:YES];
    [rightSwitch setOn:setting animated:YES];
}

//按钮点击事件
-(IBAction)buttonPressed{
    //显示操作表
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"are you sure?" 
delegate:self 
cancelButtonTitle:@"No way" 
destructiveButtonTitle:@"yes, i am sure!" 
otherButtonTitles:@"aa", @"bb", @"cc", nil];
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}

//操作表委托事件： 显示一个警报
-(void)actionSheet:(UIActionSheet *)actionSheet 
didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    if(buttonIndex != [actionSheet cancelButtonIndex]){
        NSString *msg = nil;
        if(nameField.text.length > 0){
            msg = [[NSString alloc] initWithFormat:@"you can breathe easy, %@, 
everything went ok.", nameField.text ];
        }else{
            msg = @"you can breathe easy, everything went ok.";
        }
        //警报
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"something was done!" 
message:msg 
delegate:self 
cancelButtonTitle:@"Phew" 
otherButtonTitles:nil, nil];

        [alert show];
        [alert release];
        [msg release];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// 按钮不同状态，设置不同的背景
    UIImage * buttonImageNormal = [UIImage imageNamed:@"whiteButton.png"];
    // 可拉伸图像 4.0
    UIImage *stretchableBuutonImageNormal = [buttonImageNormal 
stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [doSomethingButton setBackgroundImage:stretchableBuutonImageNormal 
forState:UIControlStateNormal];
    
    
    //按钮按下时的背景
    UIImage * buttonImagePressed = [UIImage imageNamed:@"blueButton.png"];
    UIImage * stretchableButtonImagePressed = [buttonImagePressed 
stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    [doSomethingButton setBackgroundImage:stretchableButtonImagePressed 
forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [nameField release];
    [numberField release];
    [sliderLabel release];
    [leftSwitch release];
    [rightSwitch release];
    [doSomethingButton release];
    
    [super dealloc];
}

@end
