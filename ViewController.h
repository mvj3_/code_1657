//
//  ViewController.h
//  Learning0514
//
//  Created by  apple on 13-5-14.
//  Copyright (c) 2013年 dhy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kSwitchesSegmentIndex 0

@interface ViewController : UIViewController <UIActionSheetDelegate>
{
    //文本框
    UITextField *nameField;
    UITextField *numberField;
    //标签
    UILabel *sliderLabel;
    //开关按钮
    UISwitch *leftSwitch;
    UISwitch *rightSwitch;
    //普通按钮
    UIButton *doSomethingButton;
}

@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UITextField *numberField;
@property (nonatomic, retain) IBOutlet UILabel *sliderLabel;
@property (nonatomic, retain) IBOutlet UISwitch *leftSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *rightSwitch;
@property (nonatomic, retain) IBOutlet UIButton *doSomethingButton;

//文本框输入完成时， 关闭键盘
-(IBAction)textFieldDoneEditing:(id)sender;

//点击背景时，关闭键盘
-(IBAction)backgroundTap:(id)sender;

//滑块滑动时，显示当前的值
-(IBAction)sliderChanged:(id)sender;

//分段控件，切换显示不同的控件
-(IBAction)toggleControls:(id)sender;

//开关切换on\off
-(IBAction)switchChanged:(id)sender;

//按钮点击事件
-(IBAction)buttonPressed;

@end
